package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.ComunaModel;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ComunaView extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtRegion;
	private JButton btnRegistrar;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComunaView frame = new ComunaView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ComunaView() {
		setTitle("Comuna");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 218, 123);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setBounds(10, 11, 58, 14);
		contentPane.add(lblNombre);
		
		JLabel lblRegion = new JLabel("Region:");
		lblRegion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRegion.setBounds(10, 36, 58, 14);
		contentPane.add(lblRegion);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(78, 8, 86, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtRegion = new JTextField();
		txtRegion.setBounds(78, 33, 86, 20);
		contentPane.add(txtRegion);
		txtRegion.setColumns(10);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(53, 61, 89, 23);
		contentPane.add(btnRegistrar);
		
		
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtRegion() {
		return txtRegion;
	}

	public void setTxtRegion(JTextField txtRegion) {
		this.txtRegion = txtRegion;
	}

	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}

	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}
}
