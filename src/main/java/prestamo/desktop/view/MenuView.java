package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class MenuView extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuView frame = new MenuView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 506, 335);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmCerrarSesion = new JMenuItem("Cerrar Sesion");
		mnArchivo.add(mntmCerrarSesion);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);
		
		JMenu mnRegistrar = new JMenu("Registrar");
		menuBar.add(mnRegistrar);
		
		JMenuItem mntmUsuarios = new JMenuItem("Usuarios");
		mnRegistrar.add(mntmUsuarios);
		
		JMenuItem mntmRegiones = new JMenuItem("Regiones");
		mnRegistrar.add(mntmRegiones);
		
		JMenuItem mntmComunas = new JMenuItem("Comunas");
		mnRegistrar.add(mntmComunas);
		
		JMenuItem mntmReputacion = new JMenuItem("Reputacion");
		mnRegistrar.add(mntmReputacion);
		
		JMenu mnRealizar = new JMenu("Realizar");
		menuBar.add(mnRealizar);
		
		JMenuItem mntmPrestamos = new JMenuItem("Prestamos");
		mnRealizar.add(mntmPrestamos);
		
		JMenuItem mntmPagos = new JMenuItem("Pagos");
		mnRealizar.add(mntmPagos);
		
		JMenu mnConsultar = new JMenu("Consultar");
		menuBar.add(mnConsultar);
		
		JMenuItem mntmPrestamosL = new JMenuItem("Prestamos");
		mnConsultar.add(mntmPrestamosL);
		
		JMenuItem mntmRegistros = new JMenuItem("Registros");
		mnConsultar.add(mntmRegistros);
		
		JMenuItem mntmUsuariosL = new JMenuItem("Usuarios");
		mnConsultar.add(mntmUsuariosL);
		
		JMenuItem mntmRegionesL = new JMenuItem("Regiones");
		mnConsultar.add(mntmRegionesL);
		
		JMenuItem mntmComunasL = new JMenuItem("Comunas");
		mnConsultar.add(mntmComunasL);
		
		JMenuItem mntmReputaciones = new JMenuItem("Reputaciones");
		mnConsultar.add(mntmReputaciones);
		
		JMenuItem mntmTipousuario = new JMenuItem("TipoUsuario");
		mnConsultar.add(mntmTipousuario);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
