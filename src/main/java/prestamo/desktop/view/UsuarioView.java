package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import prestamo.desktop.model.UsuarioModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UsuarioView extends JFrame {

	private JPanel contentPane;
	private JTextField anombre;
	private JTextField aapellido;
	private JTextField arut;
	private JTextField aregionid;
	private JTextField acomunaid;
	private JTextField atipo;
	private JTextField acorreo;
	private JTextField ausuario;
	private JTextField acontraseña;
	private JTextField arepetir;
	private JButton btnRegistrar;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UsuarioView frame = new UsuarioView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UsuarioView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 437, 351);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		contentPane.setBackground(SystemColor.controlHighlight);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 11, 130, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellido.setBounds(10, 36, 130, 14);
		contentPane.add(lblApellido);
		
		JLabel lblNewLabel_1 = new JLabel("RUT");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 61, 130, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblRegionid = new JLabel("Region ID");
		lblRegionid.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRegionid.setBounds(10, 86, 130, 14);
		contentPane.add(lblRegionid);
		
		JLabel lblComunaid = new JLabel("Comuna ID");
		lblComunaid.setHorizontalAlignment(SwingConstants.RIGHT);
		lblComunaid.setBounds(10, 111, 130, 14);
		contentPane.add(lblComunaid);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTipo.setBounds(10, 136, 130, 14);
		contentPane.add(lblTipo);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCorreo.setBounds(10, 161, 130, 14);
		contentPane.add(lblCorreo);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsuario.setBounds(10, 186, 130, 14);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contraseña");
		lblContrasea.setHorizontalAlignment(SwingConstants.RIGHT);
		lblContrasea.setBounds(10, 211, 130, 14);
		contentPane.add(lblContrasea);
		
		anombre = new JTextField();
		anombre.setBounds(150, 11, 180, 20);
		contentPane.add(anombre);
		anombre.setColumns(10);
		
		aapellido = new JTextField();
		aapellido.setBounds(150, 36, 180, 20);
		contentPane.add(aapellido);
		aapellido.setColumns(10);
		
		arut = new JTextField();
		arut.setBounds(150, 61, 180, 20);
		contentPane.add(arut);
		arut.setColumns(10);
		
		aregionid = new JTextField();
		aregionid.setBounds(150, 86, 180, 20);
		contentPane.add(aregionid);
		aregionid.setColumns(10);
		
		acomunaid = new JTextField();
		acomunaid.setBounds(150, 111, 180, 20);
		contentPane.add(acomunaid);
		acomunaid.setColumns(10);
		
		atipo = new JTextField();
		atipo.setBounds(150, 136, 180, 20);
		contentPane.add(atipo);
		atipo.setColumns(10);
		
		acorreo = new JTextField();
		acorreo.setBounds(150, 161, 180, 20);
		contentPane.add(acorreo);
		acorreo.setColumns(10);
		
		ausuario = new JTextField();
		ausuario.setBounds(150, 186, 180, 20);
		contentPane.add(ausuario);
		ausuario.setColumns(10);
		
		acontraseña = new JTextField();
		acontraseña.setBounds(150, 211, 180, 20);
		contentPane.add(acontraseña);
		acontraseña.setColumns(10);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*UsuarioModel oum = new UsuarioModel();
				oum.setNombre(anombre.getText());
				oum.setApellido(aapellido.getText());
				oum.setRut(arut.getText());
				oum.setRegionid(aregionid.getText());
				oum.setComunaid(acomunaid.getText());
				oum.setTipo(atipo.getText());
				oum.setCorreo(acorreo.getText());
				oum.setUsuario(ausuario.getText());
				oum.setContraseña(acontraseña.getText());
				oum.create();*/
			}
		});
		btnRegistrar.setBounds(94, 278, 89, 23);
		contentPane.add(btnRegistrar);
		
		JLabel lblNewLabel_2 = new JLabel("Repetir Contraseña");
		lblNewLabel_2.setBounds(38, 244, 102, 14);
		contentPane.add(lblNewLabel_2);
		
		arepetir = new JTextField();
		arepetir.setBounds(148, 242, 182, 20);
		contentPane.add(arepetir);
		arepetir.setColumns(10);
		
	}

	public JTextField getAnombre() {
		return anombre;
	}

	public void setAnombre(JTextField anombre) {
		this.anombre = anombre;
	}

	public JTextField getAapellido() {
		return aapellido;
	}

	public void setAapellido(JTextField aapellido) {
		this.aapellido = aapellido;
	}

	public JTextField getArut() {
		return arut;
	}

	public void setArut(JTextField arut) {
		this.arut = arut;
	}

	public JTextField getAregionid() {
		return aregionid;
	}

	public void setAregionid(JTextField aregionid) {
		this.aregionid = aregionid;
	}

	public JTextField getAcomunaid() {
		return acomunaid;
	}

	public void setAcomunaid(JTextField acomunaid) {
		this.acomunaid = acomunaid;
	}

	public JTextField getAtipo() {
		return atipo;
	}

	public void setAtipo(JTextField atipo) {
		this.atipo = atipo;
	}

	public JTextField getAcorreo() {
		return acorreo;
	}

	public void setAcorreo(JTextField acorreo) {
		this.acorreo = acorreo;
	}

	public JTextField getAusuario() {
		return ausuario;
	}

	public void setAusuario(JTextField ausuario) {
		this.ausuario = ausuario;
	}

	public JTextField getAcontraseña() {
		return acontraseña;
	}

	public void setAcontraseña(JTextField acontraseña) {
		this.acontraseña = acontraseña;
	}

	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}

	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}
	public JTextField getArepetir() {
		return arepetir;
	}

	public void setArepetir(JTextField orepetir) {
		this.arepetir= orepetir;
	}
}
