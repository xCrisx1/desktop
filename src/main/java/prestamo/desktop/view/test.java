package prestamo.desktop.view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class test extends JFrame {
	
	private JPanel contentPane;
	private JPanel panel;
	private JTextField userTextField;
	private JTextField passTextField;
	private JButton botonIniciarSesion;
	private JLabel lblClave;
	private JLabel lblUsuario;
	
	public static void main(String args[]) {
		test frame = new test();
		frame.setVisible(true);
	}
	
	public test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100,100,450,300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5,5,5,5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(10,11,414,239);
		contentPane.add(panel);
		panel.setLayout(null);
		
		userTextField = new JTextField();
		userTextField.setBounds(143,66,127,20);
		panel.add(userTextField);
		
		passTextField = new JTextField();
		passTextField.setBounds(143,97,127,20);
		panel.add(passTextField);
		
		botonIniciarSesion = new JButton();
		botonIniciarSesion.setBounds(143, 152, 117, 23);
		botonIniciarSesion.setText("iniciar sesion");
		panel.add(botonIniciarSesion);
		
		lblClave = new JLabel();
		lblClave.setBounds(53,97,97,23);
		lblClave.setText("contraseña:");
		panel.add(lblClave);
		
		lblUsuario = new JLabel();
		lblUsuario.setBounds(53,66,97,23);
		lblUsuario.setText("usuario:");
		panel.add(lblUsuario);
	}
}
