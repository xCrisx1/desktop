package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.PrestamoModel;

import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.Button;
import javax.swing.JRadioButtonMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class PrestamoView extends JFrame {

	private JPanel contentPane;
	private JTextField txtPrestamista;
	private JTextField txtUsuario;
	private JLabel lblUsuario;
	private JTextField txtfechaEntrada;
	private JTextField txtfechaSalida;
	private JLabel lblFechaEntrada;
	private JLabel lblFechaSalida;
	private JTextField txtcuotasPedidas;
	private JLabel lblCuotasPedidas;
	private JTextField txtmontoPedido;
	private JLabel lblMontoPedido;
	private JButton button;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrestamoView frame = new PrestamoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrestamoView() {
		setTitle("Prestamo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 341, 256);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPrestamista = new JLabel("Prestamista:");
		lblPrestamista.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrestamista.setBounds(10, 11, 101, 14);
		contentPane.add(lblPrestamista);
		
		txtPrestamista = new JTextField();
		txtPrestamista.setBounds(121, 8, 185, 20);
		contentPane.add(txtPrestamista);
		txtPrestamista.setColumns(10);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(120, 39, 185, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		lblUsuario = new JLabel("Usuario:");
		lblUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsuario.setBounds(10, 42, 101, 14);
		contentPane.add(lblUsuario);
		
		txtfechaEntrada = new JTextField();
		txtfechaEntrada.setBounds(120, 70, 185, 20);
		contentPane.add(txtfechaEntrada);
		txtfechaEntrada.setColumns(10);
		
		txtfechaSalida = new JTextField();
		txtfechaSalida.setBounds(120, 101, 185, 20);
		contentPane.add(txtfechaSalida);
		txtfechaSalida.setColumns(10);
		
		lblFechaEntrada = new JLabel("Fecha Entrada:");
		lblFechaEntrada.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFechaEntrada.setBounds(10, 73, 101, 14);
		contentPane.add(lblFechaEntrada);
		
		lblFechaSalida = new JLabel("Fecha Salida:");
		lblFechaSalida.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFechaSalida.setBounds(10, 104, 101, 14);
		contentPane.add(lblFechaSalida);
		
		txtcuotasPedidas = new JTextField();
		txtcuotasPedidas.setBounds(120, 132, 185, 20);
		contentPane.add(txtcuotasPedidas);
		txtcuotasPedidas.setColumns(10);
		
		lblCuotasPedidas = new JLabel("Cuotas Pedidas:");
		lblCuotasPedidas.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCuotasPedidas.setBounds(10, 135, 101, 14);
		contentPane.add(lblCuotasPedidas);
		
		txtmontoPedido = new JTextField();
		txtmontoPedido.setBounds(120, 163, 185, 20);
		contentPane.add(txtmontoPedido);
		txtmontoPedido.setColumns(10);
		
		lblMontoPedido = new JLabel("Monto Pedido:");
		lblMontoPedido.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMontoPedido.setBounds(10, 166, 101, 14);
		contentPane.add(lblMontoPedido);
		
		button = new JButton("Registrar");
		button.setBounds(141, 194, 101, 23);
		contentPane.add(button);
	}

	public JTextField getTxtPrestamista() {
		return txtPrestamista;
	}

	public void setTxtPrestamista(JTextField txtPrestamista) {
		this.txtPrestamista = txtPrestamista;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public JTextField getTxtfechaEntrada() {
		return txtfechaEntrada;
	}

	public void setTxtfechaEntrada(JTextField txtfechaEntrada) {
		this.txtfechaEntrada = txtfechaEntrada;
	}

	public JTextField getTxtfechaSalida() {
		return txtfechaSalida;
	}

	public void setTxtfechaSalida(JTextField txtfechaSalida) {
		this.txtfechaSalida = txtfechaSalida;
	}

	public JTextField getTxtcuotasPedidas() {
		return txtcuotasPedidas;
	}

	public void setTxtcuotasPedidas(JTextField txtcuotasPedidas) {
		this.txtcuotasPedidas = txtcuotasPedidas;
	}

	public JTextField getTxtmontoPedido() {
		return txtmontoPedido;
	}

	public void setTxtmontoPedido(JTextField txtmontoPedido) {
		this.txtmontoPedido = txtmontoPedido;
	}

	public JButton getButton() {
		return button;
	}

	public void setButton(JButton button) {
		this.button = button;
	}
	
	
}
