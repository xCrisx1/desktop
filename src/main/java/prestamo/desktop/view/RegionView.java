package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.RegionModel;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegionView extends JFrame {

	private JPanel contentPane;
	private JTextField anombre;
	private JButton btnNewButton;

	public JTextField getAnombre() {
		return anombre;
	}

	public void setAnombre(JTextField anombre) {
		this.anombre = anombre;
	}

	public JButton getBtnNewButton() {
		return btnNewButton;
	}

	public void setBtnNewButton(JButton btnNewButton) {
		this.btnNewButton = btnNewButton;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegionView frame = new RegionView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegionView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 258, 168);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(42, 51, 46, 14);
		contentPane.add(lblNombre);
		
		anombre = new JTextField();
		anombre.setBounds(98, 48, 86, 20);
		contentPane.add(anombre);
		anombre.setColumns(10);
		
		btnNewButton = new JButton("Registrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegionModel orm = new RegionModel();
				orm.setNombre(anombre.getText());
				orm.create();
			}
		});
		btnNewButton.setBounds(98, 79, 89, 23);
		contentPane.add(btnNewButton);
	}

}
