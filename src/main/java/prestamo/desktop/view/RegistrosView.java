package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.RegistrosModel;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegistrosView extends JFrame {

	private JPanel contentPane;
	private JTextField aid_prestamista;
	private JTextField aid_usuario;
	private JTextField afecha;
	private JTextField amonto;
	private JButton btnRegistrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistrosView frame = new RegistrosView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistrosView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 239, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID Usuario");
		lblNewLabel.setBounds(10, 14, 91, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblIdPrestamista = new JLabel("ID Prestamista");
		lblIdPrestamista.setBounds(10, 39, 91, 14);
		contentPane.add(lblIdPrestamista);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(10, 64, 91, 14);
		contentPane.add(lblFecha);
		
		JLabel lblMonto = new JLabel("Monto");
		lblMonto.setBounds(10, 89, 91, 14);
		contentPane.add(lblMonto);
		
		aid_prestamista = new JTextField();
		aid_prestamista.setBounds(111, 36, 86, 20);
		contentPane.add(aid_prestamista);
		aid_prestamista.setColumns(10);
		
		aid_usuario = new JTextField();
		aid_usuario.setBounds(111, 11, 86, 20);
		contentPane.add(aid_usuario);
		aid_usuario.setColumns(10);
		
		afecha = new JTextField();
		afecha.setBounds(111, 61, 86, 20);
		contentPane.add(afecha);
		afecha.setColumns(10);
		
		amonto = new JTextField();
		amonto.setBounds(111, 86, 86, 20);
		contentPane.add(amonto);
		amonto.setColumns(10);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegistrosModel orm = new RegistrosModel();
				orm.setId_usuario(aid_usuario.getText());
				orm.setId_prestamista(aid_prestamista.getText());
				orm.setFecha(afecha.getText());
				orm.setMonto(amonto.getText());
				orm.create();
			}
		});
		btnRegistrar.setBounds(108, 117, 89, 23);
		contentPane.add(btnRegistrar);
	}

	public JTextField getAid_prestamista() {
		return aid_prestamista;
	}

	public void setAid_prestamista(JTextField aid_prestamista) {
		this.aid_prestamista = aid_prestamista;
	}

	public JTextField getAid_usuario() {
		return aid_usuario;
	}

	public void setAid_usuario(JTextField aid_usuario) {
		this.aid_usuario = aid_usuario;
	}

	public JTextField getAfecha() {
		return afecha;
	}

	public void setAfecha(JTextField afecha) {
		this.afecha = afecha;
	}

	public JTextField getAmonto() {
		return amonto;
	}

	public void setAmonto(JTextField amonto) {
		this.amonto = amonto;
	}

	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}

	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}
}
