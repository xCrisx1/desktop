package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.ReputacionModel;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ReputacionView extends JFrame {

	private JPanel contentPane;
	private JTextField txtReputacion;
	private JTextField txtAtrasos;
	private JButton btnRegistrar;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReputacionView frame = new ReputacionView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ReputacionView() {
		setTitle("Reputacion");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 204, 126);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblReputacion = new JLabel("Reputacion:");
		lblReputacion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblReputacion.setBounds(10, 11, 68, 14);
		contentPane.add(lblReputacion);
		
		JLabel lblAtrasos = new JLabel("Atrasos:");
		lblAtrasos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAtrasos.setBounds(10, 36, 68, 14);
		contentPane.add(lblAtrasos);
		
		txtReputacion = new JTextField();
		txtReputacion.setBounds(88, 8, 86, 20);
		contentPane.add(txtReputacion);
		txtReputacion.setColumns(10);
		
		txtAtrasos = new JTextField();
		txtAtrasos.setBounds(88, 33, 86, 20);
		contentPane.add(txtAtrasos);
		txtAtrasos.setColumns(10);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(51, 61, 89, 23);
		contentPane.add(btnRegistrar);
	}

	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}

	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}

	public JTextField getTxtReputacion() {
		return txtReputacion;
	}

	public void setTxtReputacion(JTextField txtReputacion) {
		this.txtReputacion = txtReputacion;
	}

	public JTextField getTxtAtrasos() {
		return txtAtrasos;
	}

	public void setTxtAtrasos(JTextField txtAtrasos) {
		this.txtAtrasos = txtAtrasos;
	}
	
	
}
