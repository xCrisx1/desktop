package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import prestamo.desktop.model.UsuarioModel;
import prestamo.desktop.view.UsuarioView;

public class UsuarioControlador implements ActionListener{
	
	private UsuarioView form;
	
	UsuarioControlador(UsuarioView uv){
		form = uv;
		form.getBtnRegistrar().addActionListener(this);
		form.setVisible(true);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		UsuarioModel oum = new UsuarioModel();
		
		if(e.getSource() == form.getBtnRegistrar()) {
			if(!form.getAcontraseña().getText().equals(form.getArepetir().getText())) {
				System.out.println("no coincide la contraseña");
				return;
			}
			for(UsuarioModel um:oum.getlist()) {
				if(um.getUsuario().equals(form.getAusuario().getText())) {
					System.out.println("Ya existe este usuario, utilice otro");
					return;
				}
				if(um.getCorreo().equals(form.getAcorreo().getText())) {
					System.out.println("Ese correo ya esta registrado, utilice otro");
					return;
				}
				if(um.getRut().equals(form.getArut().getText())) {
					System.out.println("Ya existe un usuario registrado con ese rut, contacte a su dios para que lo renazca");
				}

			}
		oum.setNombre(form.getAnombre().getText());
		oum.setApellido(form.getAapellido().getText());
		oum.setRut(form.getArut().getText());
		oum.setRegionid(form.getAregionid().getText());
		oum.setComunaid(form.getAcomunaid().getText());
		oum.setTipo(form.getAtipo().getText());
		oum.setCorreo(form.getAcorreo().getText());
		oum.setUsuario(form.getAusuario().getText());
		oum.setContraseña(form.getAcontraseña().getText());
		oum.create();
		}	
	}
}
