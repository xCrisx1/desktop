package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.ReputacionModel;
import prestamo.desktop.view.ReputacionView;

public class ReputacionController implements ActionListener{
	
	public ReputacionView form;
	
	ReputacionController(ReputacionView rv){
		this.form = rv;
		form.getBtnRegistrar().addActionListener(this);
		form.setVisible(true);
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(e.getSource()==form.getBtnRegistrar()) {
			ReputacionModel rep = new ReputacionModel();
			rep.setRep(form.getTxtReputacion().getText());
			rep.setAtrasos(form.getTxtAtrasos().getText());
			rep.create();
		}
	}
	
	

}
