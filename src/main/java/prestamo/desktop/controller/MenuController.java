package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.view.MenuView;

public class MenuController implements ActionListener {
	
	private MenuView form;
	
	MenuController(MenuView menu){
		form = menu;
		form.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
	
}
