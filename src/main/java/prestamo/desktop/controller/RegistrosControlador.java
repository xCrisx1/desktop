package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.RegistrosModel;
import prestamo.desktop.view.RegistrosView;

public class RegistrosControlador implements ActionListener{

		private RegistrosView form;
	
		RegistrosControlador(RegistrosView rv){
			form = rv;
			form.getBtnRegistrar().addActionListener(this);
			form.setVisible(true);
		}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		RegistrosModel orm = new RegistrosModel();
		orm.setId_prestamista(form.getAid_prestamista().getText());
		orm.setId_usuario(form.getAid_usuario().getText());
		orm.setMonto(form.getAmonto().getText());
		orm.setFecha(form.getAfecha().getText());
		orm.create();
		}
}
