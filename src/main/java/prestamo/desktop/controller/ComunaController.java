package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.ComunaModel;
import prestamo.desktop.view.ComunaView;

public class ComunaController implements ActionListener {
	ComunaView form;
	
	ComunaController(ComunaView cm){
		this.form = cm;
		form.getBtnRegistrar().addActionListener(this);
		form.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == form.getBtnRegistrar()) {
			ComunaModel com = new ComunaModel();
			com.setNombre(form.getTxtNombre().getText());
			com.setRegionid(form.getTxtRegion().getText());
			
			com.create();
		}
	}
	
}
