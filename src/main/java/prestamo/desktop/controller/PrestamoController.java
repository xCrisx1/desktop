package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.PrestamoModel;
import prestamo.desktop.view.PrestamoView;

public class PrestamoController implements ActionListener {
	private PrestamoView form;
	//asd
	PrestamoController(PrestamoView pv){
		this.form = pv;
		pv.getButton().addActionListener(this);
		form.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()== form.getButton()) {
			PrestamoModel pres = new PrestamoModel();
			pres.setPrestamista_id(form.getTxtPrestamista().getText());
			pres.setUsuario_id(form.getTxtUsuario().getText());
			pres.setFechaEntrada(form.getTxtfechaEntrada().getText());
			pres.setFechaSalida(form.getTxtfechaSalida().getText());
			pres.setCuotaspedidas(form.getTxtcuotasPedidas().getText());
			pres.setMontopedido(form.getTxtmontoPedido().getText());
			
			//sacamos calculos del interes y seteamos las cuotasrestantes
			int interes = (Integer.parseInt(form.getTxtmontoPedido().getText()));
			interes = (int) (interes * 0.10);
			int montototal = Integer.parseInt(pres.getMontopedido()) + interes;
			int montoacancelar = montototal / Integer.parseInt(pres.getCuotaspedidas());
			
			pres.setMontoacancelar(Integer.toString(montoacancelar));
			pres.setCuotasrestantes(pres.getCuotaspedidas());
			pres.setMontototal(Integer.toString(montototal));
			
			pres.create();
		}
	}

}
