package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.RegionModel;
import prestamo.desktop.model.UsuarioModel;
import prestamo.desktop.view.RegionView;

public class RegionControlador implements ActionListener{

	private RegionView form;
	
	RegionControlador(RegionView rv){
		form = rv;
		form.getBtnNewButton().addActionListener(this);
		form.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		RegionModel orm = new RegionModel();
		for(RegionModel um:orm.getlist()){
			if(um.getNombre().equals(form.getAnombre().getText())) {
				System.out.println("Ya existe este una region, utilice otra");
				return ;
			}
		}
		orm.setNombre(form.getAnombre().getText());
		orm.create();
	}
}