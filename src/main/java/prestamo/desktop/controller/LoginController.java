package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.LoginModel;
import prestamo.desktop.view.LoginView;

public class LoginController implements ActionListener {
	private LoginView form;
	
	LoginController(LoginView lg){
		form = lg;
		form.getBtnIniciarSesion().addActionListener(this);
		form.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == form.getBtnIniciarSesion()) {
			LoginModel log = new LoginModel();
			
			log.setUsuario(form.getTxtUsuario().getText());
			log.setContraseña(form.getTxtContraseña().getText());
			
			log.IniciarSesion();
		}
	}

}
