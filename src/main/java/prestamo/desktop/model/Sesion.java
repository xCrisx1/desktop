package prestamo.desktop.model;

public class Sesion {
	//tipos: 1=prestamista, 2=usuario, 3=admin
	public static boolean Logged = false;
	public static int Tipo = 2;
	
	public static void CerrarSesion() {
		Logged = false;
		Tipo = 2;
	}
}
