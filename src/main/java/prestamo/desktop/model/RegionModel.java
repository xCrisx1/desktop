package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RegionModel extends DB implements Model<RegionModel>{
	
	private Integer id;
	private String nombre;

	@Override
	public boolean create() {
		try {
			PreparedStatement pst = this.getConnection().prepareStatement("INSERT INTO Region(nombre) VALUES(?)");
				pst.setString(1, this.nombre);
				pst.execute();
				System.out.println("yes");
				return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage().toString());
			e.printStackTrace();
		}return false;
	}

	public ArrayList<RegionModel> getlist(){
		ArrayList<RegionModel> list = new ArrayList<>();
		try {
			PreparedStatement pst = this.getConnection().prepareStatement("SELECT * FROM region");
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				RegionModel orm = new RegionModel();
				orm.setNombre(rs.getString("nombre"));
				list.add(orm);
			}
			return list;
		} catch (SQLException e) {
			System.out.println(e.getMessage().toString());
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public boolean update() {
		return false;
	}

	@Override
	public boolean delete() {
		return false;
	}

	@Override
	public RegionModel get() {
		return null;
	}
	//----------------------------------------------------------------
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

}
