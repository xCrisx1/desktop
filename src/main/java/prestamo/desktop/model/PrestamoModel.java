package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PrestamoModel extends DB implements Model<PrestamoModel>{
	private String id;
	private String prestamista_id;
	private String usuario_id;
	private String fechaEntrada;
	private String fechaSalida;
	private String cuotaspedidas;
	private String montoacancelar;
	private String montototal;
	private String cuotasrestantes;
	private String montopedido;
	//asd
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrestamista_id() {
		return prestamista_id;
	}
	public void setPrestamista_id(String prestamista_id) {
		this.prestamista_id = prestamista_id;
	}
	public String getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(String usuario_id) {
		this.usuario_id = usuario_id;
	}
	public String getFechaEntrada() {
		return fechaEntrada;
	}
	public void setFechaEntrada(String fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}
	public String getFechaSalida() {
		return fechaSalida;
	}
	public void setFechaSalida(String fechaSalida) {
		this.fechaSalida = fechaSalida;
	}
	public String getCuotaspedidas() {
		return cuotaspedidas;
	}
	public void setCuotaspedidas(String cuotaspedidas) {
		this.cuotaspedidas = cuotaspedidas;
	}
	public String getMontoacancelar() {
		return montoacancelar;
	}
	public void setMontoacancelar(String montoacancelar) {
		this.montoacancelar = montoacancelar;
	}
	public String getMontototal() {
		return montototal;
	}
	public void setMontototal(String montototal) {
		this.montototal = montototal;
	}
	public String getCuotasrestantes() {
		return cuotasrestantes;
	}
	public void setCuotasrestantes(String cuotasrestantes) {
		this.cuotasrestantes = cuotasrestantes;
	}
	public String getMontopedido() {
		return montopedido;
	}
	public void setMontopedido(String montopedido) {
		this.montopedido = montopedido;
	}
	public boolean create() {
		try {
			PreparedStatement ps = this.getConnection().prepareStatement("insert into Prestamo"
					+ "(prestamista_id,usuario_id,fechaentrada,fechasalida,cuotaspedidas,montoacancelar,montototal,cuotasrestantes,montopedido) "
					+ "values (?,?,?,?,?,?,?,?,?)");
			ps.setString(1, this.prestamista_id);
			ps.setString(2, this.usuario_id);
			ps.setString(3, this.fechaEntrada);
			ps.setString(4, this.fechaSalida);
			ps.setString(5, this.cuotaspedidas);
			ps.setString(6, this.montoacancelar);
			ps.setString(7, this.montototal);
			ps.setString(8, this.cuotasrestantes);
			ps.setString(9, this.montopedido);
			ps.execute();
			System.out.println("creado");
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage().toString());
		}
		
		return false;
	}
	public boolean update() {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}
	public PrestamoModel get() {
		// TODO Auto-generated method stub
		return null;
	}
}
