package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ReputacionModel extends DB implements Model<ReputacionModel> {
	private String rep;
	private String atrasos;
	
	public String getRep() {
		return rep;
	}
	public void setRep(String rep) {
		this.rep = rep;
	}
	public String getAtrasos() {
		return atrasos;
	}
	public void setAtrasos(String atrasos) {
		this.atrasos = atrasos;
	}
	@Override
	public boolean create() {
		try {
			PreparedStatement ps = this.getConnection().prepareStatement("insert into Reputacion(rep,atrasos) values (?,?)");
			ps.setString(1, this.rep);
			ps.setString(2, this.atrasos);
			ps.execute();
			System.out.println("creado");
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage().toString());
		}
		return false;
	}
	@Override
	public boolean update() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ReputacionModel get() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
