package prestamo.desktop.model;

public interface Model<T> {
	public abstract boolean create();
	public abstract boolean update();
	public abstract boolean delete();
	public abstract T get();
}
