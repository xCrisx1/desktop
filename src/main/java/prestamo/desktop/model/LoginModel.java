package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginModel extends DB{
	private String usuario;
	private String contraseña;
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	
	public void IniciarSesion() {
		try {
			PreparedStatement ps = this.getConnection().prepareStatement("select usuario,correo,contraseña,tipo from Usuario");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				if(rs.getString("usuario").equals(this.usuario) || rs.getString("correo").equals(this.usuario)) {
					if(rs.getString("contraseña").equals(this.contraseña)) {
						Sesion.Logged = true;
						Sesion.Tipo = Integer.parseInt(rs.getString("tipo"));
						System.out.println("Usuario Logueado");
						return;
					}
				}
			}
			System.out.println("Usuario Incorrecto");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage().toString());
		}
	}
}
