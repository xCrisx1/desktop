package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegistrosModel extends DB implements Model<RegistrosModel>{
	
	private Integer id;
	private String id_usuario;
	private String id_prestamista;
	private String fecha;
	private String monto;

	@Override
	public boolean create() {
		try {
			PreparedStatement pst = this.getConnection().prepareStatement("INSERT INTO Registros(id_usuario,id_prestamista,fecha,monto) VALUES(?,?,?,?)");
			pst.setString(1, this.id_usuario);
			pst.setString(2, this.id_prestamista);
			pst.setString(3, this.fecha);
			pst.setString(4, this.monto);
			pst.execute();
			System.out.println("yes");
		return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage().toString());
			e.printStackTrace();
		}return false;
		
	}

	@Override
	public boolean update() {
		return false;
	}

	@Override
	public boolean delete() {
		return false;
	}

	@Override
	public RegistrosModel get() {
		return null;
	}
	
	//-----------------------------------------------------------------
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getId_prestamista() {
		return id_prestamista;
	}

	public void setId_prestamista(String id_prestamista) {
		this.id_prestamista = id_prestamista;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	
	

}
