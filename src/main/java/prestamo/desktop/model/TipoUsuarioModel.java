package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TipoUsuarioModel extends DB implements Model<TipoUsuarioModel> {
	private String id;
	private String tipo;
	private String descripcion;
	
	@Override
	public boolean create() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public TipoUsuarioModel get() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ArrayList<TipoUsuarioModel> getList(){
		ArrayList<TipoUsuarioModel> list = new ArrayList<>();
		
		try {
			PreparedStatement ps = this.getConnection().prepareStatement("select id,tipo,descripcion from TipoUsuario");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				TipoUsuarioModel temp = new TipoUsuarioModel();
				temp.setId(rs.getString("id"));
				temp.setTipo(rs.getString("tipo"));
				temp.setDescripcion(rs.getString("descripcion"));
				list.add(temp);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage().toString());
		}
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
