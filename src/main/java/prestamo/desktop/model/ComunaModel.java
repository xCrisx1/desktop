package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ComunaModel extends DB implements Model<ComunaModel> {
	private String id;
	private String nombre;
	private String regionid;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRegionid() {
		return regionid;
	}
	public void setRegionid(String regionid) {
		this.regionid = regionid;
	}
	@Override
	public boolean create() {
		try {
			PreparedStatement ps = this.getConnection().prepareStatement("insert into Comuna(nombre,regionid) values (?,?)");
			ps.setString(1, this.nombre);
			ps.setString(2,this.regionid);
			ps.execute();
			System.out.println("creado");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage().toString());
		}
		return false;
	}
	@Override
	public boolean update() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ComunaModel get() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
