package prestamo.desktop.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioModel extends DB implements Model<UsuarioModel>{
	private Integer id;
	private String nombre;
	private String apellido;
	private String rut;
	private String regionid;
	private String comunaid;
	private String id_rep;
	private String tipo;
	private String correo;
	private String usuario;
	private String contraseña;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	//---------------------------------------------------
	@Override
	public boolean create() {
		try {
			PreparedStatement pst = this.getConnection().prepareStatement("INSERT INTO Usuario("
					+ "nombre,apellido,rut,regionid,comunaid,tipo,correo,usuario,contraseña) "
					+ "VALUES(?,?,?,?,?,?,?,?,?)");
			
			pst.setString(1, this.nombre);
			pst.setString(2, this.apellido);
			pst.setString(3, this.rut);
			pst.setString(4, this.regionid);
			pst.setString(5, this.comunaid);
			pst.setString(6, this.tipo);
			pst.setString(7, this.correo);
			pst.setString(8, this.usuario);
			pst.setString(9 ,this.contraseña);
			pst.execute();
			System.out.println("yes");
			return true;
		}catch (SQLException e) {
			System.out.println(e.getMessage().toString());
			e.printStackTrace();
		}return false;
	}
	
	public ArrayList<UsuarioModel> getlist(){
		ArrayList<UsuarioModel> list = new ArrayList<>();
		try {
			PreparedStatement pst = this.getConnection().prepareStatement("SELECT * FROM usuario");
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				UsuarioModel oum = new UsuarioModel();
				oum.setNombre(rs.getString("nombre"));
				oum.setApellido(rs.getString("apellido"));
				oum.setRut(rs.getString("rut"));
				oum.setComunaid(rs.getString("comunaid"));
				oum.setRegionid(rs.getString("regionid"));
				oum.setTipo(rs.getString("tipo"));
				oum.setCorreo(rs.getString("correo"));
				oum.setUsuario(rs.getString("usuario"));
				oum.setContraseña(rs.getString("contraseña"));
				list.add(oum);
			}
			return list;
		} catch (SQLException e) {
			System.out.println(e.getMessage().toString());
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public boolean update() {
		try {
			PreparedStatement pst = this.getConnection().prepareStatement("UPDATE Usuario SET ");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public boolean delete() {
		
		return false;
	}
	@Override
	public UsuarioModel get() {
		return null;
	}
	//-------------------------------------------------
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}//------------------------------------------------
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}//------------------------------------------------
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}//------------------------------------------------
	public String getRegionid() {
		return regionid;
	}
	public void setRegionid(String regionid) {
		this.regionid = regionid;
	}//------------------------------------------------
	public String getComunaid() {
		return comunaid;
	}
	public void setComunaid(String comunaid) {
		this.comunaid = comunaid;
	}//------------------------------------------------
	public String getId_rep() {
		return id_rep;
	}
	public void setId_rep(String id_rep) {
		this.id_rep = id_rep;
	}//------------------------------------------------
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}//------------------------------------------------
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}//------------------------------------------------
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}//------------------------------------------------
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}//------------------------------------------------
}
